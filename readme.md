## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


----------------------------------------------------------------------------------------------------------------------

## Productions App Requeriments
 
 - `PHP`
 - `Composer`
 - `MySQL`

 
## Migrate BD
 - `php artisan migrate`
 
## Seeder Running

The `Seeder` can be invoked as follows :

 -  `php artisan db:seed --class=AlbumsTableSeeder`
 -  `php artisan db:seed --class=ArtistTableSeeder`
 
## Server Running `(Optional)`
 -   `php artisan serve`

 
 
 
 


