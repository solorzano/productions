<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    //

    protected $table = 'artist';

    protected $fillable = [
        'first_name', 'last_name', 'rol_artist', 'id_album'
    ];

    public function albums()
    {
        return $this->hasMany('App\Albums', 'id_albums', 'id_album');
    }
}
