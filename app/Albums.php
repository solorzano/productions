<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Albums extends Model
{
    //

    protected $table = 'albums';

    protected $fillable = [
        'title_albums', 'date_albums', 'first_name', 'last_name', 'rol_artist', 'id_album'
    ];

    public function artist()
    {


        return $this->hasMany('App\Artist', 'id_album', 'id_albums');
    }


}
