<?php

namespace App\Http\Controllers;

use App\Albums;
use App\Artist;

use DB;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AlbumsController extends Controller
{

    public function __construct()
    {
        $this->middleware('cors');
    }
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$Albums = Albums::select('id_albums', 'title_albums', 'date_albums')->get();

        $Albums = Albums::select('id_albums', 'title_albums', 'date_albums')
            ->with('artist')
            ->get();

        if($Albums != '[]'){
            return response()->json($Albums);
        }else{
            return response()->json('Not Found..');
        }


    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //

        /*return Business::create([
            'name_business' => $request['name_business'],
            'owner_business' => $request['owner_business'],
            'address_business' => $request['address_business'],
            //'active' => $data['active'],
        ]);*/

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $albums = new Albums($request->all());

        $albums->save();

//dd($request->id_album);
        //$Albums = new Albums($request->all());

        /*$album = new Albums($request->all());
        /*$album->title_albums = $request->title_albums;
        $album->date_albums = $request->date_albums;*/

        /*$artist = new Artist();
        $artist->first_name = $request->first_name;
        $artist->last_name = $request->last_name;
        $artist->rol_artist = $request->rol_artist;
        $artist->id_album = (integer)1;
        //$artist->id_album = 1;

        //$albums = $album;
        //$album->artist()->
        $album->artist()->save($artist);*/

        //dealerships()->save($dealership);
        //$album->artist()->associate($artist);
        //$artist->albums()->save($album);


        //$Albums->setDateFormat('d/m/y');
        //dd($Albums);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $albums = Albums::select('id_albums', 'title_albums', 'date_albums')
            ->where('id_albums', '=', $id)
            ->get();

        if($albums != '[]'){
            return response()->json($albums);
        }else{
            return response()->json('Not Found..');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Albums::where('id_albums', '=', $id)->delete();

        //Albums::destroy($id);

        return response()->json(array('success' => true));
    }
}
