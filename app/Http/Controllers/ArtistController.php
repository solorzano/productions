<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Albums;
use App\Artist;
use DB;
use Mockery\CountValidator\Exception;

class ArtistController extends Controller
{

    public function __construct()
    {
        $this->middleware('cors');
    }
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $artists = Artist::select('id_artist', 'first_name', 'last_name', 'rol_artist', 'id_album')
            ->with('albums')
            ->get();

        if($artists != '[]'){
            return response()->json($artists);
        }else{
            return response()->json('Not Found..');
        }


    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //

        /*return Business::create([
            'name_business' => $request['name_business'],
            'owner_business' => $request['owner_business'],
            'address_business' => $request['address_business'],
            //'active' => $data['active'],
        ]);*/

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $Business = new Artist($request->all());

        $Business->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $artists = Artist::select('id_artist', 'first_name', 'last_name', 'rol_artist', 'id_album')
            ->with('albums')
            ->where('id_artist', '=', $id)
            ->get();

            if($artists != '[]'){
                return response()->json($artists);
            }else{
                return response()->json('Not Found..');
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Artist::where('id_artist', '=', $id)->delete();
        //Artist::destroy($id);

        return response()->json(array('success' => true));
    }
}
