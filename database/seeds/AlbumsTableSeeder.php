<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class AlbumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        //
        for ($i=0; $i < 10; $i++) {

            DB::table('albums')->insert([
                'title_albums' => $faker->firstName,
                'date_albums'  => $faker->date($format = 'Y-m-d', $max = 'now'),
                'created_at'   => $faker->dateTime($format='Y-m-d'),
                'updated_at'   => $faker->dateTime($format='Y-m-d')
            ]);
        }
    }
}
