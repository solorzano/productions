<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ArtistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        $values = array();
        for ($i=0; $i < 10; $i++) {

            DB::table('artist')->insert([
                'first_name' => $faker->firstName,
                'last_name'  => $faker->lastName,
                'rol_artist'   => $faker->randomElement(
                    $array = ['Voice','Guitar','DJ', 'Drummer', 'Bassists', 'keyboardist','producer']),
                'id_album'   => $faker->numberBetween(1,10),
                'created_at'   => $faker->dateTime($format='Y-m-d'),
                'updated_at'   => $faker->dateTime($format='Y-m-d')
            ]);
        }
    }
}
