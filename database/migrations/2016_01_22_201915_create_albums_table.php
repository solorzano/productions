<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Migrations table albums
        Schema::create('albums', function (Blueprint $table) {
            $table->increments('id_albums')->unsigned();
            $table->string('title_albums');
            $table->date('date_albums');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('albums');
    }
}
