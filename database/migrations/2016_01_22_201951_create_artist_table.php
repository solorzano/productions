<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('artist', function (Blueprint $table) {
            $table->increments('id_artist');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('rol_artist');
            $table->integer('id_album')->unsigned();
            //Foreing Keys table Albums
            $table->foreign('id_album')
                ->references('id_albums')->on('albums');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('artist');
    }
}
